package value.global.passing.com.globalvariable;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener{

    FloatingActionButton fabTwo;
    TextView receivedName, receivedEmail;
    GlobalObject globalObject;
    String name, email, editedNameString, editedEmailString;
    EditText editName, editEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /** Get global object instance */
        globalObject = (GlobalObject)getApplicationContext();

        setContentView(R.layout.activity_second);
        initViews();
        initToolbar();
        initButtonClickListener();
        populateFieldsWithGlobalValue();

    }

    private void populateFieldsWithGlobalValue() {
        name = globalObject.getName();
        email = globalObject.getEmail();
        receivedName.setText(name);
        receivedEmail.setText(email);
    }

    private void initSnackbar(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    private void initButtonClickListener() {
        fabTwo.setOnClickListener(this);
    }

    private void initViews() {
        fabTwo = (FloatingActionButton) findViewById(R.id.fabTwo);
        receivedName = (TextView)findViewById(R.id.second_activity_received_name);
        receivedEmail = (TextView)findViewById(R.id.second_activity_received_email);

        editName = (EditText)findViewById(R.id.second_activity_edit_name);
        editEmail = (EditText)findViewById(R.id.second_activity_edit_email);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.fabTwo){
            editValues();
            moveToFinalActivity();
        }

    }

    private void moveToFinalActivity() {
        Intent intent = new Intent(getBaseContext(), ThirdActivity.class);
        startActivity(intent);
    }

    private void editValues() {
        editedNameString = editName.getText().toString();
        editedEmailString = editEmail.getText().toString();
        globalObject.setName(editedNameString);
        globalObject.setEmail(editedEmailString);
    }

}
