package value.global.passing.com.globalvariable;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class FirstActivity extends AppCompatActivity implements View.OnClickListener{

    GlobalObject globalObject;
    FloatingActionButton fabOne;
    EditText mName, mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        initViews();
        initToolbar();
        initButtonClickListener();

        /**
         * Calling Application class (see application tag in AndroidManifest.xml
         */
        globalObject = (GlobalObject) getApplicationContext();
    }

    private void initSnackbar(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    private void initButtonClickListener() {
    fabOne.setOnClickListener(this);
    }

    private void initViews() {
        fabOne = (FloatingActionButton) findViewById(R.id.fabOne);
        mName = (EditText)findViewById(R.id.enter_name);
        mEmail = (EditText)findViewById(R.id.enter_email);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.fabOne){
            getAndSetValues();
            moveToNextActivity();
        }

    }

    private void moveToNextActivity() {
        Intent intent = new Intent(getBaseContext(), SecondActivity.class);
        startActivity(intent);
    }

    private void getAndSetValues() {
        String enterNamed = mName.getText().toString();
        String enterEmail = mEmail.getText().toString();
        globalObject.setName(enterNamed);
        globalObject.setEmail(enterEmail);
    }


}
