package value.global.passing.com.globalvariable;

import android.app.Application;

public class GlobalObject extends Application {

    private String name;
    private String email;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }


}
