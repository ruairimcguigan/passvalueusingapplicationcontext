package value.global.passing.com.globalvariable;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity{
    GlobalObject globalObject;
    TextView receivedName, receivedEmail;
    String finalName, finalEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        initViews();
        globalObject = (GlobalObject) getApplicationContext();
        finalName = globalObject.getName();
        finalEmail = globalObject.getEmail();

        initToolbar();
        getFinalValues();
    }

    private void getFinalValues() {

        receivedName.setText(finalName);
        receivedEmail.setText(finalEmail);
    }

    private void initViews() {
            receivedName = (TextView)findViewById(R.id.third_activity_received_name);
            receivedEmail = (TextView)findViewById(R.id.third_activity_received_email);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }



}
